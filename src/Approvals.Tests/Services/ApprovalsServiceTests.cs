﻿using EasyProjects.CoreDbTests.Stubs;
using EasyProjects.Shared.Scope;
using Approvals.Models;
using Approvals.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Approvals.Tests.Services
{
    [TestClass]
    public class ApprovalsServiceTests
    {
        private readonly EPScopeStub _scopeStub = new EPScopeStub { AccountId = 1, UserId = 1 };

        private Mock<IApprovalsRepository> mockApprovalsRepository;

        private ApprovalsService service;

        [TestInitialize]
        public void TestInitialize()
        {
            var mocker = new AutoMocker();

            mocker.Use<IEPScopeAccessor>(x => x.Current == _scopeStub);
            service = mocker.CreateInstance<ApprovalsService>();
            mockApprovalsRepository = mocker.GetMock<IApprovalsRepository>();
            mockApprovalsRepository
                .Setup(_ => _.GetApprovalsCreationInfo(It.IsAny<int[]>(), It.IsAny<int[]>()))
                .ReturnsAsync(Enumerable.Empty<ApprovalCreationInfo>());

        }

        [TestMethod]
        public async Task GetApprovalsModel_correctly_set_read_status()
        {
            var approvalsList = new List<CurrentUserApproval>
            {
                new CurrentUserApproval
                {
                    ApprovalId = 1,
                    TimeVisit = new DateTime(2019, 5, 12),
                    LastModificationDate = new DateTime(2019, 5, 11),
                    IsActive = true,
                },
                new CurrentUserApproval
                {
                    ApprovalId = 2,
                    TimeVisit = new DateTime(2019, 5, 10),
                    LastModificationDate = new DateTime(2019, 5, 11),
                    IsActive = true,
                }
            };

            var approversList = new List<ApproverInfo>
            {
                new ApproverInfo
                {
                    UserId = 1,
                    ApprovalId = 1,
                    IsCreator = true,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 2,
                    ApprovalId = 1,
                    Status = ApproverStatus.Approved,
                },
                new ApproverInfo
                {
                    UserId = 3,
                    ApprovalId = 1,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 4,
                    ApprovalId = 2,
                    IsCreator = true,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 5,
                    ApprovalId = 2,
                    Status = ApproverStatus.None,
                },
            };

            mockApprovalsRepository.Setup(_ => _.GetCurrentUserApprovals(false)).ReturnsAsync(approvalsList);
            mockApprovalsRepository.Setup(_ => _.GetApproversInfo(1, 2)).ReturnsAsync(approversList);

            var result = await service.GetApprovalsModel(false);

            Assert.IsTrue(result.ApprovalRequests.Single(_ => _.ApprovalId == 1).IsRead);
            Assert.IsFalse(result.ApprovalRequests.Single(_ => _.ApprovalId == 2).IsRead);
        }

        [TestMethod]
        public async Task GetApprovalsModel_correctly_set_creator_id_and_approvers()
        {
            var approvalsList = new List<CurrentUserApproval>
            {
                new CurrentUserApproval
                {
                    ApprovalId = 1,
                    LastModificationDate = new DateTime(2019, 5, 11),
                    IsActive = true,
                },
                new CurrentUserApproval
                {
                    ApprovalId = 2,
                    LastModificationDate = new DateTime(2019, 5, 11),
                    IsActive = true,
                }
            };

            var approversList = new List<ApproverInfo>
            {
                new ApproverInfo
                {
                    UserId = 1,
                    ApprovalId = 1,
                    IsCreator = true,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 2,
                    ApprovalId = 1,
                    Status = ApproverStatus.Approved,
                },
                new ApproverInfo
                {
                    UserId = 3,
                    ApprovalId = 1,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 4,
                    ApprovalId = 2,
                    IsCreator = true,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 5,
                    ApprovalId = 2,
                    Status = ApproverStatus.None,
                },
            };

            mockApprovalsRepository.Setup(_ => _.GetCurrentUserApprovals(false)).ReturnsAsync(approvalsList);
            mockApprovalsRepository.Setup(_ => _.GetApproversInfo(1, 2)).ReturnsAsync(approversList);

            var result = await service.GetApprovalsModel(false);

            Assert.AreEqual(1, result.ApprovalRequests.Single(_ => _.ApprovalId == 1).CreatorId);
            CollectionAssert.AreEquivalent(new[] { 2, 3 }, 
                result.ApprovalRequests.Single(_ => _.ApprovalId == 1).Approvers.Select(_ => _.UserId).ToArray());
            Assert.AreEqual(4, result.ApprovalRequests.Single(_ => _.ApprovalId == 2).CreatorId);
            CollectionAssert.AreEquivalent(new[] { 5 }, 
                result.ApprovalRequests.Single(_ => _.ApprovalId == 2).Approvers.Select(_ => _.UserId).ToArray());
        }

        [TestMethod]
        public async Task GetApprovalsModel_returns_users_without_duplicates()
        {
            var approvalsList = new List<CurrentUserApproval>
            {
                new CurrentUserApproval
                {
                    ApprovalId = 1,
                    LastModificationDate = new DateTime(2019, 5, 11),
                    IsActive = true,
                },
                new CurrentUserApproval
                {
                    ApprovalId = 2,
                    LastModificationDate = new DateTime(2019, 5, 11),
                    IsActive = true,
                }
            };

            var approversList = new List<ApproverInfo>
            {
                new ApproverInfo
                {
                    UserId = 1,
                    ApprovalId = 1,
                    IsCreator = true,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 2,
                    ApprovalId = 1,
                    Status = ApproverStatus.Approved,
                },
                new ApproverInfo
                {
                    UserId = 3,
                    ApprovalId = 1,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 3,
                    ApprovalId = 2,
                    IsCreator = true,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 1,
                    ApprovalId = 2,
                    Status = ApproverStatus.None,
                },
            };

            mockApprovalsRepository.Setup(_ => _.GetCurrentUserApprovals(false)).ReturnsAsync(approvalsList);
            mockApprovalsRepository.Setup(_ => _.GetApproversInfo(1, 2)).ReturnsAsync(approversList);

            var result = await service.GetApprovalsModel(false);

            CollectionAssert.AreEquivalent(new[] { 1, 2, 3 }, result.Users.Select(_ => _.Id).ToArray());
        }

        [TestMethod]
        public async Task GetApprovalsModel_returns_unread_flag_is_user_not_visited_details()
        {
            var approvalsList = new List<CurrentUserApproval>
            {
                new CurrentUserApproval
                {
                    ApprovalId = 1,
                    LastModificationDate = new DateTime(2019, 5, 11),
                    IsActive = true,
                },
            };

            var approversList = new List<ApproverInfo>
            {
                new ApproverInfo
                {
                    UserId = 1,
                    ApprovalId = 1,
                    IsCreator = true,
                    Status = ApproverStatus.None,
                },
                new ApproverInfo
                {
                    UserId = 2,
                    ApprovalId = 1,
                    Status = ApproverStatus.Approved,
                }
            };

            mockApprovalsRepository.Setup(_ => _.GetCurrentUserApprovals(false)).ReturnsAsync(approvalsList);
            mockApprovalsRepository.Setup(_ => _.GetApproversInfo(1)).ReturnsAsync(approversList);

            var result = await service.GetApprovalsModel(false);

            CollectionAssert.AreEquivalent(new[] { 1 }, result.ApprovalRequests.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetApprovalsModel_calls_GetApprovalsCreationInfo_with_missing_inactive_data()
        {

        }

        [TestMethod]
        public async Task GetApprovalsModel_with_inprogress_flag_filters_inactive_readed_approvals()
        {

        }

        [TestMethod]
        public async Task GetApprovalsModel_with_inprogress_flag_sets_new_approval_date_for_inactive_approvals()
        {

        }

        [TestMethod]
        public async Task GetApprovalsModel_with_completed_flag_sets_new_approval_date_for_inactive_approvals()
        {

        }

        [TestMethod]
        public async Task GetApprovalsModel_correcly_select_closest_approval_date_for_inactive_approvals()
        {

        }

        [TestMethod]
        public async Task GetApprovalsModel_adds_creator_user_to_list_for_inactive_approvals()
        {

        }
    }
}
