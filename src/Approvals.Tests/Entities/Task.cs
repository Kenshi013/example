﻿using Dapper.Contrib.Extensions;
using System;

namespace Approvals.Tests.Entities
{
    [Table("TT_Tasks")]
    public class Task
    {
        public int AccountId { get; set; }
        public int ProjectId { get; set; }
        public int TaskId { get; set; }
        public string Name { get; set; }
        public int TaskStatusId { get; set; }
        public int TaskTypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public int PriorityId { get; set; }
    }
}
