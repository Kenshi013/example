﻿using Dapper.Contrib.Extensions;
using System;

namespace Approvals.Tests.Entities
{
    [Table("TT_MessageBoardOptions")]
    public class MessageBoardOptions
    {
        public int AccountId { get; set; }
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? TaskId { get; set; }
        public int? ProjectId { get; set; }
        public DateTime? TimeVisit { get; set; }
    }
}
