﻿using Dapper.Contrib.Extensions;
using System;

namespace Approvals.Tests.Entities
{
    [Table("TT_Messages")]
    public class Message
    {
        public int AccountId { get; set; }

        [Key]
        public int MessageId { get; set; }

        public int? TaskId { get; set; }

        public int? ProjectId { get; set; }

        public int? UserId { get; set; }

        public DateTime PostDate { get; set; } = DateTime.UtcNow;

        public string MessageText { get; set; } = string.Empty;

        public DateTime? EditedAt { get; set; }

        public int? EditedByUserId { get; set; }

        public string EditedByName { get; set; }
    }
}
