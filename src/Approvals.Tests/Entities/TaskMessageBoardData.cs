﻿using Dapper.Contrib.Extensions;
using System;

namespace Approvals.Tests.Entities
{

    [Table("TT_TaskMessageBoardData")]
    public class TaskMessageBoardData
    {
        public int AccountId { get; set; }
        public int TaskId { get; set; }
        public DateTime MessagesLastModificationDate { get; set; }
    }
}
