﻿using Dapper.Contrib.Extensions;
using System;

namespace Approvals.Tests.Entities
{

    [Table("TT_ProjectMessageBoardData")]
    public class ProjectMessageBoardData
    {
        public int AccountId { get; set; }
        public int ProjectId { get; set; }
        public DateTime MessagesLastModificationDate { get; set; }
    }
}
