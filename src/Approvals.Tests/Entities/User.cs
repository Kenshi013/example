﻿using Dapper.Contrib.Extensions;

namespace Approvals.Tests.Entities
{
    [Table("TT_Users")]
    public class User
    {
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public int? CustomerId { get; set; }
        public int RoleId { get; set; }
        public string Login { get; set; }
        public decimal HourlyRate { get; set; }
        public string EMail { get; set; }
        public string Name { get; set; }
    }
}
