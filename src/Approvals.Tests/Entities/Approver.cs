﻿using Dapper.Contrib.Extensions;
using Approvals.Models;

namespace Approvals.Tests.Entities
{

    [Table("TT_Approvers")]
    public class Approver
    {
        public int AccountId { get; set; }
        [Key]
        public int ApproverId { get; set; }
        public int ApprovalId { get; set; }
        public int MessageId { get; set; }
        public int? TaskId { get; set; }
        public int? ProjectId { get; set; }
        public int UserId { get; set; }
        public bool IsCreator { get; set; }
        public bool IsInterested { get; set; }
        public ApproverStatus Status { get; set; }
    }
}
