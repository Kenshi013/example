﻿using Dapper.Contrib.Extensions;
using System;

namespace Approvals.Tests.Entities
{
    [Table("TT_Projects")]
    public class Project
    {
        public int AccountId { get; set; }
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public int ProjectStatusId { get; set; }
        public int PriorityId { get; set; }
        public int CreatorId { get; set; }
        public DateTime CreationDate { get; set; }
        public int? CustomerId { get; set; }
    }
}
