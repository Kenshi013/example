﻿using Dapper.Contrib.Extensions;

namespace Approvals.Tests.Entities
{
    [Table("TT_Approvals")]
    public class Approval
    {
        public int AccountId { get; set; }
        [Key]
        public int ApprovalId { get; set; }
        public int MessageId { get; set; }
        public bool IsActive { get; set; }
    }
}
