﻿using EasyProjects.CoreDbTests.Entities;
using Approvals.Tests.Entities;
using System;

namespace Approvals.Tests.Builders
{
    class MessageBuilder : BaseBuilder<Message, CoreDbEntities, MessageBuilder>
    {
        internal MessageBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override Message CreateEntityWithDefaultValues()
        {
            return new Message
            {
                AccountId = Settings.DefaultAccountId,
                PostDate = DateTime.UtcNow,
                MessageText = string.Empty
            };
        }

        public MessageBuilder ForProject(int projectId) => Set(() => Entity.ProjectId = projectId);
        public MessageBuilder ForProject(Project project) => ForProject(project.ProjectId);

        public MessageBuilder ForTask(int taskId) => Set(() => Entity.TaskId = taskId);
        public MessageBuilder ForTask(Task task) => ForTask(task.TaskId);

        public MessageBuilder WithText(string text) => Set(() => Entity.MessageText = text);

        public MessageBuilder WithPostDate(DateTime date) => Set(() => Entity.PostDate = date);

        protected override void OnInserting()
        {
            // link message with new project if no ref task/project was specified
            if (Entity.ProjectId == null && Entity.TaskId == null)
            {
                Entity.ProjectId = Entities.AddProject.WithAccount(Entity.AccountId).Create().ProjectId;
            }
        }
    }
}
