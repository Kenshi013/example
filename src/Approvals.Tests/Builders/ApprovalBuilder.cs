﻿using EasyProjects.CoreDbTests.Entities;
using Approvals.Models;
using Approvals.Tests.Entities;
using System;
using Approver = Approvals.Tests.Entities.Approver;

namespace Approvals.Tests.Builders
{

    class ApprovalBuilder : BaseBuilder<Approval, CoreDbEntities, ApprovalBuilder>
    {
        internal ApprovalBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override Approval CreateEntityWithDefaultValues()
        {
            return new Approval
            {
                AccountId = Settings.DefaultAccountId,
                IsActive = true
            };
        }

        public ApprovalBuilder ForMessage(int messageId) => Set(() => Entity.MessageId = messageId);
        public ApprovalBuilder ForMessage(Message message) => Set(() => {
            Entity.MessageId = message.MessageId;
        });

        public ApprovalBuilder SetIsActive(bool isActive) => Set(() => Entity.IsActive = isActive);

        public ApprovalBuilder WithAccount(int accountId) => Set(() => Entity.AccountId = accountId);
    }
}
