﻿using EasyProjects.CoreDbTests.Entities;
using Approvals.Tests.Entities;
using System;

namespace Approvals.Tests.Builders
{

    class ProjectMessageBoardDataBuilder : BaseBuilder<ProjectMessageBoardData, CoreDbEntities, ProjectMessageBoardDataBuilder>
    {
        internal ProjectMessageBoardDataBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override ProjectMessageBoardData CreateEntityWithDefaultValues()
        {
            return new ProjectMessageBoardData
            {
                AccountId = Settings.DefaultAccountId,
            };
        }

        public ProjectMessageBoardDataBuilder ForProject(int projectId) => Set(() => Entity.ProjectId = projectId);
        public ProjectMessageBoardDataBuilder ForProject(Project project) => ForProject(project.ProjectId);

        public ProjectMessageBoardDataBuilder WithLMD(DateTime date) => Set(() => Entity.MessagesLastModificationDate = date);

        public ProjectMessageBoardDataBuilder WithAccount(int accountId) => Set(() => Entity.AccountId = accountId);
    }
}
