﻿using EasyProjects.CoreDbTests.Entities;
using Approvals.Tests.Entities;
using System;

namespace Approvals.Tests.Builders
{

    class MessageBoardOptionsBuilder : BaseBuilder<MessageBoardOptions, CoreDbEntities, MessageBoardOptionsBuilder>
    {
        internal MessageBoardOptionsBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override MessageBoardOptions CreateEntityWithDefaultValues()
        {
            return new MessageBoardOptions
            {
                AccountId = Settings.DefaultAccountId,
            };
        }

        public MessageBoardOptionsBuilder ForProject(int projectId) => Set(() => Entity.ProjectId = projectId);
        public MessageBoardOptionsBuilder ForProject(Project project) => ForProject(project.ProjectId);

        public MessageBoardOptionsBuilder ForTask(int taskId) => Set(() => Entity.TaskId = taskId);
        public MessageBoardOptionsBuilder ForTask(Task task) => ForTask(task.TaskId);

        public MessageBoardOptionsBuilder ForUser(int userId) => Set(() => Entity.UserId = userId);

        public MessageBoardOptionsBuilder ForUser(User user) => Set(() => {
            Entity.UserId = user.UserId;
        });

        public MessageBoardOptionsBuilder WithVisitTime(DateTime? date) => Set(() => Entity.TimeVisit = date);

        public MessageBoardOptionsBuilder WithAccount(int accountId) => Set(() => Entity.AccountId = accountId);
    }
}
