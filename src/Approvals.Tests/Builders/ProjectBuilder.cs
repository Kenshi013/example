﻿using EasyProjects.CoreDbTests.Entities;
using Approvals.Tests.Entities;
using System;

namespace Approvals.Tests.Builders
{
    class ProjectBuilder : BaseBuilder<Project, CoreDbEntities, ProjectBuilder>
    {
        public ProjectBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override Project CreateEntityWithDefaultValues()
        {
            return new Project
            {
                AccountId = Settings.DefaultAccountId,
                ProjectId = 1,
                ProjectStatusId = 1,
                PriorityId = 1,
                CreatorId = 1,
                Name = Guid.NewGuid().ToString(),
                CreationDate = DateTime.UtcNow
            };
        }

        public ProjectBuilder WithAccount(int accountId) => Set(() => this.Entity.AccountId = accountId);
        public ProjectBuilder WithName(string name) => Set(() => this.Entity.Name = name);
        public ProjectBuilder WithStatus(int statusId) => Set(() => this.Entity.ProjectStatusId = statusId);
        public ProjectBuilder WithCustomer(int customerId)=> Set(() => this.Entity.CustomerId = customerId);

        protected override void OnInserting()
        {
            this.Entity.ProjectId = Entities.GenerateId(nameof(Project), this.Entity.AccountId);
        }
    }
}
