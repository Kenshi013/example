﻿using System;
using EasyProjects.CoreDbTests.Entities;
using Approvals.Tests.Entities;

namespace Approvals.Tests.Builders
{
    class UserBuilder : BaseBuilder<User, CoreDbEntities, UserBuilder>
    {
        public UserBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override User CreateEntityWithDefaultValues()
        {
            return new User
            {
                AccountId = 1,
                RoleId = 1,
                Login = Guid.NewGuid().ToString(),
                HourlyRate = 1,
                EMail = Guid.NewGuid().ToString() + "@test.test",
            };
        }

        public UserBuilder WithAccount(int accountId) => Set(() => this.Entity.AccountId = accountId);

        public UserBuilder WithName(string name) => Set(() => this.Entity.Name = name);

        protected override void OnInserting()
        {
            this.Entity.UserId = Entities.GenerateId(nameof(User), this.Entity.AccountId);
        }
    }
}
