﻿using EasyProjects.CoreDbTests.Entities;
using Approvals.Models;
using Approvals.Tests.Entities;
using Approver = Approvals.Tests.Entities.Approver;
using User = Approvals.Tests.Entities.User;

namespace Approvals.Tests.Builders
{

    class ApproverBuilder : BaseBuilder<Approver, CoreDbEntities, ApproverBuilder>
    {
        internal ApproverBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override Approver CreateEntityWithDefaultValues()
        {
            return new Approver
            {
                AccountId = Settings.DefaultAccountId,
                IsCreator = false,
                UserId = 1,
                IsInterested = true,
            };
        }
        public ApproverBuilder ForMessage(int messageId) => Set(() => Entity.MessageId = messageId);

        public ApproverBuilder ForApproval(int approvalId) => Set(() => Entity.ApprovalId = approvalId);
        public ApproverBuilder ForApproval(Approval approval) => Set(() => {
            Entity.ApprovalId = approval.ApprovalId;
            Entity.MessageId = approval.MessageId;
        });

        public ApproverBuilder ForProject(int projectId) => Set(() => Entity.ProjectId = projectId);
        public ApproverBuilder ForProject(Project project) => ForProject(project.ProjectId);

        public ApproverBuilder ForTask(int taskId) => Set(() => Entity.TaskId = taskId);
        public ApproverBuilder ForTask(Task task) => ForTask(task.TaskId);

        public ApproverBuilder ForUser(int userId) => Set(() => Entity.UserId = userId);
        public ApproverBuilder ForUser(User user) => Set(() => {
            Entity.UserId = user.UserId;
        });
        public ApproverBuilder AsCreator() => Set(() => Entity.IsCreator = true);

        public ApproverBuilder SetInterested(bool isInterested) => Set(() => Entity.IsInterested = isInterested);

        public ApproverBuilder SetStatus(ApproverStatus status) => Set(() => Entity.Status = status);

        public ApproverBuilder WithAccount(int accountId) => Set(() => Entity.AccountId = accountId);
    }
}
