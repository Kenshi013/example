﻿using EasyProjects.CoreDbTests.Entities;
using Approvals.Tests.Entities;
using System;

namespace Approvals.Tests.Builders
{

    class TaskMessageBoardDataBuilder : BaseBuilder<TaskMessageBoardData, CoreDbEntities, TaskMessageBoardDataBuilder>
    {
        internal TaskMessageBoardDataBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override TaskMessageBoardData CreateEntityWithDefaultValues()
        {
            return new TaskMessageBoardData
            {
                AccountId = Settings.DefaultAccountId,
            };
        }

        public TaskMessageBoardDataBuilder ForTask(int taskId) => Set(() => Entity.TaskId = taskId);
        public TaskMessageBoardDataBuilder ForTask(Task task) => ForTask(task.TaskId);

        public TaskMessageBoardDataBuilder WithLMD(DateTime date) => Set(() => Entity.MessagesLastModificationDate = date);

        public TaskMessageBoardDataBuilder WithAccount(int accountId) => Set(() => Entity.AccountId = accountId);
    }
}
