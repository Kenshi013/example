﻿using EasyProjects.CoreDbTests;

namespace Approvals.Tests.Builders
{
    class CoreDbEntities : CoreDbEntitiesBase
    {
        public CoreDbEntities(CoreDbEntitiesSettings settings) : base(settings)
        {
        }

        public TaskBuilder AddTask => new TaskBuilder(this);
        public ProjectBuilder AddProject => new ProjectBuilder(this);
        public MessageBuilder AddMessage => new MessageBuilder(this);
        public UserBuilder AddUser => new UserBuilder(this);
        public ApprovalBuilder AddApproval => new ApprovalBuilder(this);
        public ApproverBuilder AddApprover => new ApproverBuilder(this);
        public MessageBoardOptionsBuilder AddMessageBoardOptions => new MessageBoardOptionsBuilder(this);
        public ProjectMessageBoardDataBuilder AddProjectMessageBoardData => new ProjectMessageBoardDataBuilder(this);
        public TaskMessageBoardDataBuilder AddTaskMessageBoardData => new TaskMessageBoardDataBuilder(this);
    }
}
