﻿using EasyProjects.CoreDbTests.Entities;
using Approvals.Tests.Entities;
using System;

namespace Approvals.Tests.Builders
{
    class TaskBuilder : BaseBuilder<Task, CoreDbEntities, TaskBuilder>
    {
        public TaskBuilder(CoreDbEntities entities) : base(entities)
        {
        }

        protected override Task CreateEntityWithDefaultValues()
        {
            return new Task
            {
                AccountId = Settings.DefaultAccountId,
                TaskId = 1,
                TaskStatusId = 1,
                TaskTypeId = 1,
                PriorityId = 1,
                Name = Guid.NewGuid().ToString(),
                CreationDate = DateTime.UtcNow
            };
        }

        public TaskBuilder WithAccount(int accountId) => Set(() => this.Entity.AccountId = accountId);
        public TaskBuilder WithName(string name) => Set(() => this.Entity.Name = name);
        public TaskBuilder WithStatus(int statusId) => Set(() => this.Entity.TaskStatusId = statusId);
        public TaskBuilder WithProject(int projectId) => Set(() => this.Entity.ProjectId = projectId);
        public TaskBuilder WithProject(Project project) => Set(() => this.Entity.ProjectId = project.ProjectId);
        public TaskBuilder WithTaskType(int taskTypeId) => Set(() => this.Entity.TaskTypeId = taskTypeId);

        protected override void OnInserting()
        {
            if (Entity.ProjectId == 0)
            {
                Entity.ProjectId = Entities.AddProject.WithAccount(Entity.AccountId).Create().ProjectId;
            }

            this.Entity.TaskId = Entities.GenerateId(nameof(Task), this.Entity.AccountId);
        }
    }
}
