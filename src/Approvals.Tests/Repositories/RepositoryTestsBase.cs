﻿using EasyProjects.CoreDbTests;
using EasyProjects.CoreDbTests.Stubs;
using EasyProjects.Shared.Scope;
using EasyProjects.TestHelpers;
using Approvals.Tests.Builders;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Approvals.Tests.Repositories
{
    public class RepositoryTestsBase<TService, TImplementation> : CoreDbTestsBase<TService, TImplementation>
        where TService : class
        where TImplementation : class, TService
    {
        internal static int accountId = 1;

        internal CoreDbEntities CoreDbEntities { get; set; }


        internal TestsSettings Settings => TestsConfiguration.BindTo<TestsSettings>();
        protected override string GetConnectionString() => Settings.ConnectionString;

        internal IEPScope ScopeStub = new EPScopeStub { AccountId = accountId, UserId = 1 };

        [TestInitialize]
        public override void TestInitialize()
        {
            base.TestInitialize();

            CoreDbEntities = new CoreDbEntities(new CoreDbEntitiesSettings(GetConnectionString()));
            ServiceCollection.AddSingleton(Mock.Of<IEPScopeAccessor>(x => x.Current == ScopeStub));
        }
    }
}
