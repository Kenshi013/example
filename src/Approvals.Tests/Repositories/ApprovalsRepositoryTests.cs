﻿using Approvals.Models;
using Approvals.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Approvals.Tests.Repositories
{
    [TestClass]
    public class ApprovalsRepositoryTests : RepositoryTestsBase<IApprovalsRepository, ApprovalsRepository>
    {

        [TestMethod]
        public async Task GetCurrentUserApprovals_for_inprogress_return_interested_approvals()
        {
            var project1 = CoreDbEntities.AddProject.Create();
            var project2 = CoreDbEntities.AddProject.Create();
            var message1 = CoreDbEntities.AddMessage.ForProject(project1).Create();
            var message2 = CoreDbEntities.AddMessage.ForProject(project2).Create();
            var approval1 = CoreDbEntities.AddApproval.ForMessage(message1).Create();
            var apr1 = CoreDbEntities.AddApprover.ForApproval(approval1).ForUser(1).ForProject(project1).Create();
            var approval2 = CoreDbEntities.AddApproval.ForMessage(message2).Create();
            var apr2 = CoreDbEntities.AddApprover.ForApproval(approval2).SetInterested(false).ForUser(1).ForProject(project2).Create();


            var result = await Service.GetCurrentUserApprovals(isInprogress: true);
            CollectionAssert.AreEquivalent(new[] { approval1.ApprovalId }, result.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_for_inprogress_return_approvals_which_was_recreated()
        {
            var user = CoreDbEntities.AddUser.Create();
            var project = CoreDbEntities.AddProject.Create();
            var message1 = CoreDbEntities.AddMessage.ForProject(project).Create();
            var message2 = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval1 = CoreDbEntities.AddApproval.ForMessage(message1).SetIsActive(false).Create();
            var apr1 = CoreDbEntities.AddApprover.ForApproval(approval1).ForUser(1).ForProject(project).Create();
            var approval2 = CoreDbEntities.AddApproval.ForMessage(message2).Create();
            var apr2 = CoreDbEntities.AddApprover.ForApproval(approval2).ForUser(user).ForProject(project).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);
            CollectionAssert.AreEquivalent(new[] { approval1.ApprovalId }, result.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_for_inprogress_return_approvals_which_is_inactive_but_messageboard_was_never_opened()
        {
            var project = CoreDbEntities.AddProject.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).SetIsActive(false).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForProject(project).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);
            CollectionAssert.AreEquivalent(new[] { approval.ApprovalId }, result.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_for_inprogress_return_approvals_which_is_inactive_but_was_not_read()
        {
            var project = CoreDbEntities.AddProject.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).SetIsActive(false).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).SetInterested(false).ForProject(project).Create();
            var mbo = CoreDbEntities.AddMessageBoardOptions.ForProject(project).ForUser(1).WithVisitTime(DateTime.UtcNow.AddMinutes(-1)).Create();
            var pmdb = CoreDbEntities.AddProjectMessageBoardData.ForProject(project).WithLMD(DateTime.UtcNow).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);
            CollectionAssert.AreEquivalent(new[] { approval.ApprovalId }, result.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_for_inprogress_return_approvals_where_user_is_creator()
        {
            var project = CoreDbEntities.AddProject.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).AsCreator().Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);
            CollectionAssert.AreEquivalent(new[] { apr.ApprovalId }, result.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_for_inprogress_return_approvals_where_user_is_approver()
        {
            var project = CoreDbEntities.AddProject.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);
            CollectionAssert.AreEquivalent(new[] { apr.ApprovalId }, result.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_for_history_return_approvals_in_which_user_is_not_interested()
        {
            var project = CoreDbEntities.AddProject.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).SetIsActive(true).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForProject(project).SetInterested(false).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: false);
            CollectionAssert.AreEquivalent(new[] { approval.ApprovalId }, result.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_for_history_return_approvals_which_is_inactive_and_read()
        {
            var project = CoreDbEntities.AddProject.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).SetIsActive(false).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForProject(project).SetInterested(false).Create();
            var mbo = CoreDbEntities.AddMessageBoardOptions.ForProject(project).ForUser(1).WithVisitTime(DateTime.UtcNow.AddMinutes(1)).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: false);
            CollectionAssert.AreEquivalent(new[] { approval.ApprovalId }, result.Select(_ => _.ApprovalId).ToArray());
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_correctly_selects_time_visit_for_project()
        {
            var visitTime = DateTime.UtcNow.Date;
            var project = CoreDbEntities.AddProject.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForProject(project).Create();
            var mbo = CoreDbEntities.AddMessageBoardOptions.ForProject(project).ForUser(1).WithVisitTime(visitTime).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);

            var resultTimeVisit = result.First().TimeVisit.Value;
            Assert.AreEqual(visitTime, resultTimeVisit);
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_correctly_selects_time_visit_for_task()
        {
            var visitTime = DateTime.UtcNow.Date;
            var project = CoreDbEntities.AddProject.Create();
            var task = CoreDbEntities.AddTask.WithProject(project).Create();
            var message = CoreDbEntities.AddMessage.ForTask(task).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForTask(task).Create();
            var mbo = CoreDbEntities.AddMessageBoardOptions.ForTask(task).ForUser(1).WithVisitTime(visitTime).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);

            var resultTimeVisit = result.First().TimeVisit.Value;
            Assert.AreEqual(visitTime, resultTimeVisit);
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_correctly_selects_lmd_for_message_for_project()
        {
            var lmd = DateTime.UtcNow.Date;
            var project = CoreDbEntities.AddProject.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForProject(project).Create();
            var pmdb = CoreDbEntities.AddProjectMessageBoardData.ForProject(project).WithLMD(lmd).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);

            var resultLmd = result.First().LastModificationDate;
            Assert.AreEqual(lmd, resultLmd);
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_correctly_selects_lmd_for_message_for_task()
        {
            var lmd = DateTime.UtcNow.Date;
            var project = CoreDbEntities.AddProject.Create();
            var task = CoreDbEntities.AddTask.WithProject(project).Create();
            var message = CoreDbEntities.AddMessage.ForTask(task).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForTask(task).Create();
            var tmdb = CoreDbEntities.AddTaskMessageBoardData.ForTask(task).WithLMD(lmd).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);

            var resultLmd = result.First().LastModificationDate;
            Assert.AreEqual(lmd, resultLmd);
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_data_mapping_for_project()
        {
            var lmd = DateTime.UtcNow.Date;
            var project = CoreDbEntities.AddProject.WithName("Super project").Create();
            var message = CoreDbEntities.AddMessage.WithText("textMessage").ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForProject(project).Create();
            var pmdb = CoreDbEntities.AddProjectMessageBoardData.ForProject(project).WithLMD(lmd).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);

            var uApproval = result.First();

            Assert.AreEqual("Super project", uApproval.ProjectName);
            Assert.AreEqual(project.ProjectId, uApproval.ProjectId);
            Assert.IsNull(uApproval.TaskId);
            Assert.AreEqual(message.MessageId, uApproval.MessageId);
            Assert.AreEqual("textMessage", uApproval.MessageText);
            Assert.IsNull(uApproval.TimeVisit);
            Assert.AreEqual(lmd, uApproval.LastModificationDate);
            Assert.AreEqual(approval.ApprovalId, uApproval.ApprovalId);
        }

        [TestMethod]
        public async Task GetCurrentUserApprovals_data_mapping_for_task()
        {
            var lmd = DateTime.UtcNow.Date;
            var project = CoreDbEntities.AddProject.Create();
            var task = CoreDbEntities.AddTask.WithProject(project).WithName("Super task").Create();
            var message = CoreDbEntities.AddMessage.WithText("textMessage").ForTask(task).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(1).ForTask(task).Create();
            var tmdb = CoreDbEntities.AddTaskMessageBoardData.ForTask(task).WithLMD(lmd).Create();

            var result = await Service.GetCurrentUserApprovals(isInprogress: true);

            var uApproval = result.First();

            Assert.AreEqual("Super task", uApproval.TaskName);
            Assert.AreEqual(task.TaskId, uApproval.TaskId);
            Assert.IsNull(uApproval.ProjectId);
            Assert.AreEqual(message.MessageId, uApproval.MessageId);
            Assert.AreEqual("textMessage", uApproval.MessageText);
            Assert.IsNull(uApproval.TimeVisit);
            Assert.AreEqual(lmd, uApproval.LastModificationDate);
            Assert.AreEqual(approval.ApprovalId, uApproval.ApprovalId);
        }

        [TestMethod]
        public async Task GetApproversInfo_returns_all_approvers_for_selected_approvals()
        {
            var project = CoreDbEntities.AddProject.Create();
            var user1 = CoreDbEntities.AddUser.Create();
            var user2 = CoreDbEntities.AddUser.Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr1 = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(user1).Create();
            var apr2 = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(user2).AsCreator().Create();

            var result = await Service.GetApproversInfo(approval.ApprovalId);

            CollectionAssert.AreEquivalent(new[] { user1.UserId, user2.UserId }, result.Select(_ => _.UserId).ToArray());
        }

        [TestMethod]
        public async Task GetApproversInfo_data_mapping()
        {
            var project = CoreDbEntities.AddProject.Create();
            var user = CoreDbEntities.AddUser.WithName("Super user").Create();
            var message = CoreDbEntities.AddMessage.ForProject(project).Create();
            var approval = CoreDbEntities.AddApproval.ForMessage(message).Create();
            var apr = CoreDbEntities.AddApprover.ForApproval(approval).ForUser(user).SetStatus(ApproverStatus.Approved).Create();

            var result = await Service.GetApproversInfo(approval.ApprovalId);

            var approverInfo = result.First();

            Assert.AreEqual(approval.ApprovalId, approverInfo.ApprovalId);
            Assert.AreEqual(false, approverInfo.IsCreator);
            Assert.AreEqual("Super user", approverInfo.Name);
            Assert.AreEqual(ApproverStatus.Approved, approverInfo.Status);
            Assert.AreEqual(user.UserId, approverInfo.UserId);
        }
    }
}
