﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Approvals.Services;
using Approvals.Models;

namespace Approvals.Controllers
{
    [Route("/api")]
    [ApiController]
    [Authorize]
    public class ApprovalsController : ControllerBase
    {
        private readonly IApprovalsService approvalsService;

        public ApprovalsController(
            IApprovalsService approvalsService)
        {
            this.approvalsService = approvalsService;
        }

        [HttpPost]
        [Route("inprogress")]
        public Task<ApprovalsModel> GetInProgressApprovals()
        {
            return approvalsService.GetApprovalsModel(isInProgress: true);
        }

        [HttpPost]
        [Route("history")]
        public Task<ApprovalsModel> GetCompletedApprovals()
        {
            return approvalsService.GetApprovalsModel(isInProgress: false);
        }
    }
}