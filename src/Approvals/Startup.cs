﻿using EasyProjects.Shared;
using Approvals.Services;
using LogicSoftware.EasyProjects.DbConnectionProvider.Client.EpDatabase;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Approvals
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public virtual void AddAuthentication(IServiceCollection services)
        {
            services.SetupDefaultMvcConfiguration(Configuration);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            this.AddAuthentication(services);

            services.AddHealthChecks();
            services.AddEpDatabaseClient(Configuration["EasyProjects:DBConnectionProviderEndpoint"]);

            services.AddTransient<IApprovalsRepository, ApprovalsRepository>();
            services.AddTransient<IApprovalsService, ApprovalsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHealthChecks("/api/ping");
            //app.ConfigureJsonExceptionHandler(env);
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
