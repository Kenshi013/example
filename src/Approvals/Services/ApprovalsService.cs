﻿using EasyProjects.Shared.Scope;
using Approvals.Helpers;
using Approvals.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Approvals.Services
{
    public class ApprovalsService : IApprovalsService
    {
        private readonly IApprovalsRepository approvalsRepository;

        public ApprovalsService(
            IApprovalsRepository approvalsRepository)
        {
            this.approvalsRepository = approvalsRepository;
        }

        public async Task<ApprovalsModel> GetApprovalsModel(bool isInProgress)
        {
            var approvals = await approvalsRepository.GetCurrentUserApprovals(isInProgress);

            var canceledApprovals = approvals.Where(_ => !_.IsActive).ToList();

            var approvalCreationsTask = approvalsRepository.GetApprovalsCreationInfo(
                canceledApprovals.Where(_ => _.TaskId.HasValue).Select(_ => _.TaskId.Value).ToArray(),
                canceledApprovals.Where(_ => _.ProjectId.HasValue).Select(_ => _.ProjectId.Value).ToArray()
            );

            var approversTask = approvalsRepository.GetApproversInfo(approvals.Select(_ => _.ApprovalId).ToArray());
            var users = new List<User>();
            var result = new ApprovalsModel();
            
            var approvalCreations = await approvalCreationsTask;
            var approvers = await approversTask;

            foreach (var approval in approvals)
            {
                var approversForApproval = approvers.Where(_ => _.ApprovalId == approval.ApprovalId);
                var creatorId = approversForApproval.First(_ => _.IsCreator).UserId;
                var approverUsers = approversForApproval.Select(_ => new Approver
                {
                    UserId = _.UserId,
                    Status = _.Status,
                }).Where(_ => _.UserId != creatorId);

                bool isRead = true;
                DateTime lmd = approval.LastModificationDate;
                int? newApproveCreatorId = null;
                if (approval.IsActive)
                {
                    isRead = !(!approval.TimeVisit.HasValue || approval.LastModificationDate > approval.TimeVisit.Value);
                }
                else
                {
                    var closestApproval = approvalCreations
                        .Where(_ => _.TaskId == approval.TaskId && _.ProjectId == approval.ProjectId)
                        .Where(_ => _.PostDate > approval.PostDate)
                        .OrderBy(_ => _.PostDate)
                        .First();

                    if (isInProgress)
                    {
                        if(approval.TimeVisit.HasValue && (closestApproval.PostDate <= approval.TimeVisit))
                        {
                            continue;
                        }
                        lmd = closestApproval.PostDate;
                        isRead = false;
                    }
                    else
                    {
                        lmd = closestApproval.PostDate;
                    }
                    newApproveCreatorId = closestApproval.CreatorId;
                    users.Add(new User
                    {
                        Id = closestApproval.CreatorId,
                        Name = closestApproval.Name
                    });
                }

                result.ApprovalRequests.Add(new ApprovalRequest
                {
                    ApprovalId = approval.ApprovalId,
                    CreatorId = creatorId,
                    Approvers = approverUsers,
                    EntityName = approval.ProjectName ?? approval.TaskName,
                    MessageId = approval.MessageId,
                    MessageText = approval.MessageText,
                    ProjectId = approval.ProjectId,
                    TaskId = approval.TaskId,
                    LastModificationDate = lmd,
                    IsRead = isRead,
                    CanceledByUserId = newApproveCreatorId,
                    IsCanceled = !approval.IsActive
                });
                users.AddRange(approversForApproval.Select(_ => new User
                {
                    Id = _.UserId,
                    Name = _.Name
                }));
            }

            result.Users = users.DistinctBy(_ => _.Id).ToList();

            return result;
        }
    }
}
