﻿using Dapper;
using EasyProjects.Shared.Scope;
using Approvals.Models;
using LogicSoftware.EasyProjects.DbConnectionProvider.Client.EpDatabase;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Approvals.Services
{

    public class ApprovalsRepository: IApprovalsRepository
    {
        private readonly IEpDatabaseSqlExecutor sqlExecutor;
        private readonly IEPScopeAccessor scopeAccessor;
        private IEPScope scope => scopeAccessor.Current;

        public ApprovalsRepository(
            IEpDatabaseSqlExecutor sqlExecutor,
            IEPScopeAccessor scopeAccessor)
        {
            this.sqlExecutor = sqlExecutor;
            this.scopeAccessor = scopeAccessor;
        }

        public Task<IEnumerable<CurrentUserApproval>> GetCurrentUserApprovals(bool isInprogress)
        {
            return sqlExecutor.RunAsync(scope.AccountId, dbConnection =>
            {
                string inProgressFilterQuery = "IsInterested = 1 OR (IsActive = 0 AND (TimeVisit IS NULL OR LastModificationDate > TimeVisit))";
                string historyFilterQuery = "(IsActive = 0 AND (TimeVisit > PostDate)) OR (IsActive = 1 AND IsInterested = 0)";
                string sQuery = $@"
WITH ApprovalInfo AS(
	SELECT
		@UserId AS UserID,
		@AccountId AS AccountId,
        apr.ApprovalId,
		apr.ProjectID,
		apr.TaskID,
		apr.IsInterested,
		aprvls.IsActive,
		pr.Name AS ProjectName,
		ts.Name AS TaskName,
		apr.MessageID,
        mes.MessageText,
		mes.PostDate,
		CASE WHEN apr.ProjectID is not null then mbo.TimeVisit else tmbo.TimeVisit end as TimeVisit,
		CASE WHEN apr.ProjectID is not null then pmd.LastModificationDate else tmd.LastModificationDate end as LastModificationDate
	FROM TT_Approvers AS apr
		LEFT JOIN TT_MessageBoardOptions AS mbo ON mbo.ProjectID = apr.ProjectID AND apr.UserID = mbo.UserID AND mbo.AccountId = apr.AccountId
		LEFT JOIN TT_ProjectMessageBoardData AS pmd ON pmd.ProjectID = apr.ProjectID AND pmd.AccountId = apr.AccountId
		LEFT JOIN TT_MessageBoardOptions AS tmbo ON tmbo.TaskID = apr.TaskID AND apr.UserID = tmbo.UserID AND tmbo.AccountId = apr.AccountId
		LEFT JOIN TT_TaskMessageBoardData AS tmd ON tmd.TaskID = apr.TaskID AND tmd.AccountId = apr.AccountId
		LEFT JOIN TT_Projects AS pr ON pr.ProjectID = apr.ProjectID AND pr.AccountId = apr.AccountId
		LEFT JOIN TT_Tasks AS ts ON ts.TaskID = apr.TaskID AND ts.AccountId = apr.AccountId
        LEFT JOIN TT_Messages mes ON mes.AccountId = apr.AccountId AND mes.MessageId = apr.MessageID
		LEFT JOIN TT_Approvals aprvls ON aprvls.AccountId = apr.AccountId AND aprvls.ApprovalId = apr.ApprovalId
	WHERE apr.UserID = @UserId AND apr.AccountId = @AccountId
)
SELECT * FROM ApprovalInfo WHERE {(isInprogress ? inProgressFilterQuery : historyFilterQuery)}";

                return dbConnection.QueryAsync<CurrentUserApproval>(sQuery, new
                {
                    scope.AccountId,
                    scope.UserId,
                });
            });
        }

        public Task<IEnumerable<ApproverInfo>> GetApproversInfo(params int[] approvalsIds)
        {
            return sqlExecutor.RunAsync(scope.AccountId, dbConnection =>
            {
                string sQuery = @"
SELECT 
apr.ApprovalId,
apr.UserID,
apr.Status,
apr.IsCreator,
usr.Name
FROM TT_Approvers apr
LEFT JOIN TT_Users AS usr ON usr.UserID = apr.UserID AND usr.AccountId = apr.AccountId
WHERE apr.AccountId = @AccountId AND apr.ApprovalId IN @approvalsIds";

                return dbConnection.QueryAsync<ApproverInfo>(sQuery, new
                {
                    scope.AccountId,
                    approvalsIds,
                });
            });
        }

        public Task<IEnumerable<ApprovalCreationInfo>> GetApprovalsCreationInfo(int[] taskIds, int[] projectIds)
        {
            if (taskIds.Length == 0 && projectIds.Length == 0) {
                return Task.FromResult(Enumerable.Empty<ApprovalCreationInfo>());
            } 
            return sqlExecutor.RunAsync(scope.AccountId, dbConnection =>
            {
                var tasksFilter = taskIds.Length > 0 ? "apr.TaskId IN @taskIds" : null;
                var projectsFilter = projectIds.Length > 0 ? "apr.ProjectId IN @projectIds" : null;
                var filters = string.Empty;

                if (tasksFilter == null)
                {
                    filters = projectsFilter;
                } else if (projectsFilter == null)
                {
                    filters = tasksFilter;
                } else
                {
                    filters = $"({tasksFilter} OR {projectsFilter})";
                }

                string sQuery = $@"
	SELECT
        apr.UserId as CreatorId,
        apr.TaskId,
        apr.ProjectId,
        us.Name,
        mes.MessageId,
		mes.PostDate
	FROM TT_Approvers AS apr
        LEFT JOIN TT_Messages mes ON mes.AccountId = apr.AccountId AND mes.MessageId = apr.MessageID
        LEFT JOIN TT_Users us ON us.AccountId = apr.AccountId AND us.UserId = apr.UserId
	WHERE {filters} AND apr.AccountId = @AccountId AND apr.IsCreator = 1";

                return dbConnection.QueryAsync<ApprovalCreationInfo>(sQuery, new
                {
                    scope.AccountId,
                    taskIds,
                    projectIds
                });
            });
        }
    }
}
