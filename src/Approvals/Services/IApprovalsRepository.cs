﻿using Approvals.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Approvals.Services
{
    public interface IApprovalsRepository
    {
        Task<IEnumerable<ApproverInfo>> GetApproversInfo(params int[] approvalsIds);
        Task<IEnumerable<CurrentUserApproval>> GetCurrentUserApprovals(bool isInprogress);
        Task<IEnumerable<ApprovalCreationInfo>> GetApprovalsCreationInfo(int[] taskIds, int[] projectIds);
    }
}
