﻿using System.Threading.Tasks;
using Approvals.Models;

namespace Approvals.Services
{
    public interface IApprovalsService
    {
        Task<ApprovalsModel> GetApprovalsModel(bool isInProgress);
    }
}