﻿using System.Collections.Generic;

namespace Approvals.Models
{
    public class ApprovalsModel
    {
        public ApprovalsModel()
        {
            ApprovalRequests = new List<ApprovalRequest>();
            Users = new List<User>();
        }
        public List<ApprovalRequest> ApprovalRequests { get; set; }
        public List<User> Users { get; set; }
    }
}
