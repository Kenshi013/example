﻿using System;
using System.Collections.Generic;

namespace Approvals.Models
{

    public class ApprovalRequest
    {
        public int CreatorId { get; set; }
        public int MessageId { get; set; }
        public int ApprovalId { get; set; }
        public int? TaskId { get; set; }
        public int? ProjectId { get; set; }
        public string MessageText { get; set; }
        public string EntityName { get; set; }
        public IEnumerable<Approver> Approvers { get; set; }
        public DateTime LastModificationDate { get; set; }
        public bool IsRead { get; set; }
        public bool IsCanceled { get; set; }
        public int? CanceledByUserId { get; set; }
    }
}
