﻿using System;
using System.Collections.Generic;

namespace Approvals.Models
{
    public class Approver
    {
        public int UserId { get; set; }
        public ApproverStatus Status { get; set; }
    }
}
