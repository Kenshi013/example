﻿namespace Approvals.Models
{
    public class ApproverInfo
    {
        public int UserId { get; set; }
        public int ApprovalId { get; set; }
        public bool IsCreator { get; set; }
        public ApproverStatus Status { get; set; }
        public string Name { get; set; }
    }
}
