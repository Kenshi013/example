﻿using System;

namespace Approvals.Models
{
    public class ApprovalCreationInfo
    {
        public int MessageId { get; set; }
        public int CreatorId { get; set; }
        public string Name { get; set; }
        public int? TaskId { get; set; }
        public int? ProjectId { get; set; }
        public DateTime PostDate { get; set; }
    }
}
