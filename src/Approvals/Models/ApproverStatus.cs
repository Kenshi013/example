﻿namespace Approvals.Models
{

    public enum ApproverStatus
    {
        None = 0,
        Approved = 1,
        Rejected = 2
    }
}
