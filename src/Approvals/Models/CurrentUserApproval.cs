﻿using System;

namespace Approvals.Models
{
    public class CurrentUserApproval
    {
        public int UserId { get; set; }
        public int ApprovalId { get; set; }
        public DateTime PostDate { get; set; }
        public int MessageId { get; set; }
        public string MessageText { get; set; }
        public int? TaskId { get; set; }
        public int? ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string TaskName { get; set; }
        public DateTime LastModificationDate { get; set; }
        public DateTime? TimeVisit { get; set; }
        public bool IsInterested { get; set; }
        public bool IsActive { get; set; }
    }
}
