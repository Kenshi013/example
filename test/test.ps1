function Execute([scriptblock]$script){
    & $script

    if($LastExitCode -ne 0){
        throw "ERROR: The command '$($script.ToString())' returned a code: $($LastExitCode)"
    }
}

Execute({docker-compose up -d sqlserver})
Execute({docker-compose build})
Execute({docker-compose up --exit-code-from approvals_tests approvals_tests})
Execute({docker-compose down})